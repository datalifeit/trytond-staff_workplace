# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .workplace import (WorkPlace, Employee, Calendar, CalendarDay,
    WorkPlaceCalendar)
from .time_clock import TimeClockPrintStart, TimeClockPrint


def register():
    Pool.register(
        Calendar,
        WorkPlace,
        WorkPlaceCalendar,
        Employee,
        CalendarDay,
        module='staff_workplace', type_='model')
    Pool.register(
        TimeClockPrintStart,
        module='staff_workplace', type_='model',
        depends=['company_time_clock'])
    Pool.register(
        TimeClockPrint,
        module='staff_workplace', type_='wizard',
        depends=['company_time_clock'])
