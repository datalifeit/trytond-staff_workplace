========================
Staff Workplace scenario
========================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company

Install sale unit load::

    >>> config = activate_modules('staff_workplace')

Get models::

    >>> Country = Model.get('country.country')
    >>> Calendar = Model.get('staff.calendar')

Create country and subdivision with wrong code::

    >>> country = Country()
    >>> country.name = 'Spain'
    >>> country.code = 'EJ'
    >>> subdv = country.subdivisions.new()
    >>> subdv.name = 'Murcia'
    >>> subdv.code = 'MX'
    >>> subdv.type = 'city'
    >>> country.save()

Create calendar::

    >>> calendar = Calendar()
    >>> calendar.year = 2024
    >>> calendar.country = country
    >>> calendar.subdivision = country.subdivisions[0]
    >>> calendar.save()
    >>> len(calendar.days)
    0
    >>> calendar.click('import_holidays')
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: Country code "EJ" does not exist or is invalid. - 
    >>> country.code = 'ES'
    >>> country.save()
    >>> calendar.click('import_holidays')
    >>> calendar.reload()
    >>> len(calendar.days)
    9
    >>> subdv = country.subdivisions[0]
    >>> subdv.code = 'MC'
    >>> subdv.save()
    >>> calendar.click('import_holidays')
    >>> calendar.reload()
    >>> len(calendar.days)
    12